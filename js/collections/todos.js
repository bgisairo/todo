"use strict";

var app = app || {};

var TodoList = Backbone.Collection.extend({
	//reference to this collection's model
	model: app.Todo,

	localStorage: new Backbone.LocalStorage('todos-backbone'),

	//filter to all completed
	completed: function (todo) {
		return this.filter(function (todo) {
			return todo.get('completed');
		});
	},

	remaining: function () {
		//filter to unfinished items
		return this.without.apply(this, this.completed());
	},

	//keep the todos sequential
	nextOrder: function () {
		if (!this.length) {
			return 1;
		}
		return this.last().get('order') + 1;
	},

	comparator: function (todo) {
		return todo.get('order');
	}

});
//create a global collection of todos
app.Todos = new TodoList();