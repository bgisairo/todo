"use strict";

var app = app || {};

//basic todo model with title, order and completed attributes

app.Todo = Backbone.Model.extend({
	//ensure that atleast the title and completed keys are present
	defaults: {
		title: '',
		completed: false
	},

	toggle: function () {
		this.save({
			completed: !this.get('completed')
		});
	}
});